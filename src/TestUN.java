
// import de librerias de runtime de ANTLR

import org.antlr.v4.runtime.*;

import org.antlr.v4.runtime.tree.*;

import java.io.File;

/**
 * Created by trossky on 6/14/15.
 */
public class TestUN {

    public static void main(String[] args) throws Exception {
    try{
        // crear un analizador léxico que se alimenta a partir de la entrada (archivo o consola)
        Java8Lexer lexer;
        if (args.length>0)
            lexer = new Java8Lexer(new ANTLRFileStream(args[0]));
        else
            lexer = new Java8Lexer(new ANTLRInputStream(System.in));

        lexer.removeErrorListeners();
        lexer.addErrorListener(new Java8ErrorLexico());
            // Identificar al analizador léxico como fuente de tokens para el sintactico

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        // Crear el analizador sintáctico que se alimenta a partir del buffer de tokens
        Java8Parser parser = new Java8Parser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(new Java8ErrorSintactico());

        ParseTree tree = parser.compilationUnit(); // comienza el análisis en la regla inicial
        System.out.print("\033[32m");
        ParseTreeWalker walker= new ParseTreeWalker();
        walker.walk(new Java8TraductorBase(), tree);
        System.out.print("\033[30m\n");
      // System.out.println(tree.toStringTree(parser)); // imprime el árbol de derivación en forma textual
    } catch (Exception e){
        System.err.println("Error (Test): " + e);
    }

    }


}


