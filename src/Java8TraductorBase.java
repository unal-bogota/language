

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by trossky on 6/14/15.
 */

public class Java8TraductorBase extends  Java8BaseListener {
    //____________________________     Archivo Fuente
    String ruta = "traduccion.py";
    String nameFun="";
    String incrementUni="";
    String increment="";
    String vlrReturn="";
    File archivo = new File(ruta);
    //------------------------------------
    private Map<String, String> tablaArgumentPrint = new HashMap<String,String>();
    private Map<String, String> tablaDeclara = new HashMap<String, String>();
    private Map<String, String> tablaNameFun = new HashMap<String, String>();
    private Map<String, String> tablaAsignation= new HashMap<String, String>();
    private Map<String, String> tablaCiclos= new HashMap<String, String>();

    private Map<String, String> callFun = new HashMap<String, String>();

    @Override
    public void enterCompilationUnit(Java8Parser.CompilationUnitContext ctx) {
        super.enterCompilationUnit(ctx);

        try {
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, false));
            bw.write("\t#************************************************************************ \n");
            bw.write("\t#           Traductor de Java a Python                                  * \n");
            bw.write("\t# Este es un script traducido directamente de un archivo.java           * \n");
            bw.write("\t#     Traductor Creado por:                                             * \n");
            bw.write("\t#                Garcia Luis F.                                         * \n");
            bw.write("\t#                Salazar Daniel.                                        * \n");
            bw.write("\t#      Universidad Nacional de Colombia Sede Bogota                     * \n");
            bw.write("\t#      Facultaldad de Ingenieria, Departamento de Sistemas              * \n");
            bw.write("\t#************************************************************************ \n");
            bw.newLine();

            bw.close();
        } catch (Exception e) {
        }
        ;

    }

    @Override
    public void exitCompilationUnit(Java8Parser.CompilationUnitContext ctx) {
        try {

            callFun();
        } catch (Exception e) {
        }
        ;
    }


    @Override
    public void exitMethodHeader(Java8Parser.MethodHeaderContext ctx) {
        super.enterMethodHeader(ctx);

        tablaNameFun.put(String.valueOf(ctx.methodDeclarator().Identifier()), "NameFunction");

        try {
                BufferedWriter bw;
                bw = new BufferedWriter(new FileWriter(archivo, true));
                if (!(ctx.methodDeclarator().Identifier().getText().equals("main"))){
                    bw.write("def ");
                    bw.write(ctx.methodDeclarator().Identifier().getText());
                    bw.write("( ):");
                    bw.write("\n\t");
                }else{

                }

                bw.close();
            }catch (Exception e){

            }


    }
    @Override
    public void enterLocalVariableDeclaration(Java8Parser.LocalVariableDeclarationContext ctx) {
        super.enterLocalVariableDeclaration(ctx);
        try{
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, true));

            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                System.out.println(entry.getKey());
                String temp = "main";
                if (!tablaNameFun.isEmpty()) {
                    if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {

                        if ((ctx.unannType().getText().equals("int")) || (ctx.unannType().getText().equals("Integer"))) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationInt");

                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=0");
                            bw.write("\n\t");
                        }
                        if (ctx.unannType().getText().equals("float") || (ctx.unannType().getText().equals("double")) || (ctx.unannType().getText().equals("Float")) || (ctx.unannType().getText().equals("Double"))) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationFloat");
                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=0.0");
                            bw.write("\n\t");
                        }
                        if (ctx.unannType().getText().equals("String") || (ctx.unannType().getText().equals("char"))) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationChar");
                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=\"\"");
                            bw.write("\n\t");
                        }
                        if (ctx.unannType().getText().equals("ArrayList") ) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationArray");
                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=[]");
                            bw.write("\n\t");
                        }

                    }
                    else{
                        if ((ctx.unannType().getText().equals("int")) || (ctx.unannType().getText().equals("Integer"))) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationInt");
                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=0");
                            bw.write("\n");
                        }
                        if (ctx.unannType().getText().equals("float") || (ctx.unannType().getText().equals("double")) || (ctx.unannType().getText().equals("Float")) || (ctx.unannType().getText().equals("Double"))) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationFloat");

                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=0.0");
                            bw.write("\n");
                        }
                        if (ctx.unannType().getText().equals("String") || (ctx.unannType().getText().equals("char"))) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationChar");
                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=\"\"");
                            bw.write("\n");
                        }
                        if (ctx.unannType().getText().equals("ArrayList") ) {
                            tablaDeclara.put(String.valueOf(ctx.variableDeclaratorList().getText()), "LocalDeclarationArray");
                            bw.write(ctx.variableDeclaratorList().getText());
                            bw.write("=[]");
                            bw.write("\n");
                        }

                    }

                }
            }
        bw.close();

        }catch (Exception e){

        }
    }

    @Override
    public void enterAssignment(Java8Parser.AssignmentContext ctx) {
        super.enterAssignment(ctx);
        tablaAsignation.put(String.valueOf(ctx.getText()), "LocalAsignation");
        try{
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, true));

            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                String temp = "main";
                if (!tablaNameFun.isEmpty()) {
                    if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {
                        bw.write(ctx.getText());
                        bw.write("\n\t");
                    }
                    else {
                        bw.write(ctx.getText());
                        bw.write("\n");
                    }
                }
            }
            bw.close();

        }catch (Exception e){

        }

    }

    @Override
    public void exitMethodInvocation(Java8Parser.MethodInvocationContext ctx) {
        super.enterMethodInvocation(ctx);
        tablaArgumentPrint.put(String.valueOf(ctx.argumentList().getText()), "ListaArgumentos");

        try{
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, true));
            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                String temp = "main";
                if (!tablaNameFun.isEmpty()) {
                    if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {


                        bw.write("print ");
                        bw.write(ctx.argumentList().getText());


                        bw.write("\n\t");
                    }
                    else {
                        bw.write("print ");
                        bw.write(ctx.argumentList().getText());
                        bw.write("\n");

                    }
                }
            }
            bw.close();

        }catch (Exception e){

        }
    }

    @Override
    public void enterWhileStatement(Java8Parser.WhileStatementContext ctx) {
        super.exitWhileStatement(ctx);
        tablaCiclos.put(String.valueOf(ctx.expression().getText()), "Ciclos");
        try{
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, true));
            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                String temp = "main";
                if (!tablaNameFun.isEmpty()) {
                    if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {
                        bw.write("while (");
                        bw.write(ctx.expression().getText());
                        bw.write("):\n\t\t");



                    }
                    else {
                        bw.write("while (");
                        bw.write(ctx.expression().getText());
                        bw.write("):\n\t");

                    }
                }
            }
            bw.close();

        }catch (Exception e){

        }


    }

    @Override
    public void exitPostIncrementExpression(Java8Parser.PostIncrementExpressionContext ctx) {
        super.exitPostIncrementExpression(ctx);
        incrementUni=ctx.postfixExpression().getText();

        try{
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, true));
            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                String temp = "main";
                if (!tablaNameFun.isEmpty()) {
                    if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {
                            bw.write("\t");
                            bw.write(incrementUni);
                            bw.write("+=1");
                            bw.write("\n\t");

                    }
                    else {
                        bw.write("\t");
                        bw.write(incrementUni);
                        bw.write("+=1");
                        bw.write("\n");
                    }
                }
            }
            bw.close();

        }catch (Exception e){

        }

    }


    @Override
    public void enterReturnStatement(Java8Parser.ReturnStatementContext ctx) {
        super.enterReturnStatement(ctx);
        vlrReturn=ctx.expression().getText();
    }

    @Override
    public void enterIfThenStatement(Java8Parser.IfThenStatementContext ctx) {
        super.enterIfThenStatement(ctx);
        try {
            BufferedWriter bw;
            bw = new BufferedWriter(new FileWriter(archivo, true));
            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                String temp = "main";
                if (!tablaNameFun.isEmpty()) {
                    if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {
                        bw.write("if ( ");
                        bw.write(ctx.expression().getText());
                        bw.write(" ):");
                        bw.write("\n\t\t");
                    }else {
                        bw.write("if ( ");
                        bw.write(ctx.expression().getText());
                        bw.write(" ):");
                        bw.write("\n\t");
                    }
                }
            }
            bw.close();
        }catch (Exception e){
        }
    }


    public void BodyMain() throws IOException {
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(archivo, true));



        if (!tablaCiclos.isEmpty()){
            for (Map.Entry<String, String> entry : tablaCiclos.entrySet()) {
                bw.write("while (");
                bw.write(entry.getKey());
                bw.write("):\n\t");

                for (Map.Entry<String, String> entr : tablaArgumentPrint.entrySet()){
                    bw.write("print ");
                    bw.write(entr.getKey());
                    bw.write("\n\t");
                    if (incrementUni!=""){
                        bw.write(incrementUni);
                        bw.write("+=1");
                    }
                    bw.write("\n\t");
                    tablaArgumentPrint.remove(entr.getKey());
                    break;
                }
                bw.write("\n");

            }
        }

        bw.write("\n");
        bw.close();
    }

    public void callFun() throws IOException {
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(archivo, true));
        bw.newLine();
        if (!callFun.isEmpty()) {
            for (Map.Entry<String, String> entry : callFun.entrySet()) {

                bw.write("print ");
                bw.write(entry.getKey());
                bw.write("()\n");
            }
        }
        bw.write("\n");
        bw.close();
    }

    public void BodyFun() throws IOException {
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(archivo, true));

        if (!tablaCiclos.isEmpty()){
            for (Map.Entry<String, String> entry : tablaCiclos.entrySet()) {
                bw.write("while (");
                bw.write(entry.getKey());
                bw.write("):\n\t\t");
                for (Map.Entry<String, String> entr : tablaArgumentPrint.entrySet()){
                    bw.write("print ");
                    bw.write(entr.getKey());
                    bw.write(" \n\t\t");
                    if (incrementUni!=""){
                        bw.write(incrementUni);
                        bw.write("+=1");
                    }

                    bw.write("\n\t\t");
                    tablaArgumentPrint.remove(entr.getKey());
                    break;
                }
                bw.write("\n\t");
            }
        }

        bw.close();
    }


    @Override
    public void exitMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {
        try {

            for (Map.Entry<String, String> entry : tablaNameFun.entrySet()) {
                System.out.println(entry.getKey());
                String temp = "main";
                if (!tablaNameFun.isEmpty()){
                if ((entry.getValue().equals("NameFunction")) && !(entry.getKey().equals(temp))) {
                    callFun.put(String.valueOf(entry.getKey()), "NameFunction");


                    if (vlrReturn!=""){
                        BufferedWriter bw;
                        bw = new BufferedWriter(new FileWriter(archivo, true));
                        bw.write("return ");
                        bw.write(vlrReturn);
                        bw.write("\n");
                        bw.close();
                    }


                }else {


                }
                }
            }
            tablaNameFun.clear();
            tablaArgumentPrint.clear();
            tablaDeclara.clear();
            tablaAsignation.clear();
            tablaCiclos.clear();
            incrementUni="";
            vlrReturn="";

        } catch (Exception e) {
        }
    }


}