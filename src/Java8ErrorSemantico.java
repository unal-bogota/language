import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.BitSet;

/**
 * Created by trossky on 6/15/15.
 */
public class Java8ErrorSemantico extends BaseErrorListener {


    @Override
    public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {
        super.reportAmbiguity(recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs);
        System.out.printf( "\033[31m>>>Error Semantico: línea: %d:%d >>> %s", startIndex, stopIndex);
        System.out.print("\033[30m");
    }

    @Override
    public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {
        super.reportAttemptingFullContext(recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs);
        System.out.printf( "\033[31m>>>Error Semantico: línea: %d:%d >>> %s", startIndex, stopIndex);
        System.out.print("\033[30m");
    }

    @Override
    public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {
        super.reportContextSensitivity(recognizer, dfa, startIndex, stopIndex, prediction, configs);
        System.out.printf( "\033[31m>>>Error Semantico: línea: %d:%d >>> %s", startIndex, stopIndex);
        System.out.print("\033[30m");
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
        //System.out.printf("\nError Syntactico : linea = %d, columna = %d -> %s", linea, charPositionInLine, msg.concat(" Agregar elemento faltante"));
        System.out.printf( "\033[31m>>>Error Semantico: línea: %d:%d >>> %s", line, charPositionInLine, msg.concat(" Agregar o quitar elemento indicado.\n"));
        System.out.print("\033[30m");
    }
}
