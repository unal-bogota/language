﻿MANUAL DE COMPILACIÓN Y USO DE LA APLICACIÓN:

1. Se debe descargar el proyecto traductor Java a Python.
2. Luego se debe pasar a algún IDE, Eclipse, NetBeans, Intellij Idea, entre otros.
3. Una vez en el respectivo IDE, se compilan todos los .java que contiene nuestro proyecto,
   ya sea a través de consola o compilando archivo con archivo.
	a. Por consola se debe estar parado en la carpeta que contiene el proyecto, luego
	   digitar el comando "javac *.java", enter y estarán compiladas todas las clases.
	b. A través de correr el proyecto, se debe compilar cada clase con el boton "run"
	   del IDE.
4. Luego de tener las clases compiladas, se crea la clase java que va a tener el programa
   que se quiere traducir (debe ser un programa básico, que tenga lo que soporta el traductor).
5. Finalmente se generará el archivo "traducción.py", el cual contiene el programa traducido a
   Lenguaje de programación Python. Lo puede compilar y ejecutar y le correrá perfectamente.
6. Ha traducido el programa de java a python, reduciendo de una manera bastante evidente, un
   número considerable de líneas.

  
